/**
 * Author Niels
 * Date 02-12-2021
 * Version 1.0
 */

import java.util.Scanner;

public class Main {
    private static Scanner sc = new Scanner(System.in);

    /**
     * her i create 1 objects for eatch gas pumps
     */
    private static FuelPump pump1 = new FuelPump();
    private static FuelPump pump2 = new FuelPump();
    private static FuelPump pump3 = new FuelPump();
    private static FuelPump pump4 = new FuelPump();

    private static int fuelTank = 1000;

    /**
     * her in the main i have a menu to navigate the system and make choices
     * @param args
     */
    public static void main(String[] args) {
        do {
            System.out.println("Choose option");
            System.out.println("1: refuel tank");
            System.out.println("2: show Counters");
            System.out.println("3: open pumps");
            System.out.println("4: exit system");
            System.out.print("Choice: ");

            String choice = sc.next();

            switch (choice){
                case "1":
                    refuelTank();
                    break;
                case "2":
                    showCounter();
                    break;
                case "3":
                    pumpManage();
                    break;
                case "4":
                    System.exit(1);
                    break;
                default:
                    System.out.println("Wrong Input");
                    break;
            }
        }while (true);
    }

    /**
     * in showCounter the current tank amount together with the amount of gas each gas pump has used
     */
    private static void showCounter (){
        System.out.println("\nFuel Tank: " + fuelTank);
        System.out.println("Pum 1: " + pump1.getCounter());
        System.out.println("Pum 2: " + pump2.getCounter());
        System.out.println("Pum 3: " + pump3.getCounter());
        System.out.println("Pum 4: " + pump4.getCounter() + "\n");
    }

    /**
     * in refuelTank it will refuel the tank for the missing amount
     */
    private static void refuelTank(){
        int fuelMax = 1000;
        int tank = fuelTank;
        int temp;

        temp = fuelMax - tank;
        System.out.println("\nMissing Fuel = " + temp);
        temp +=tank;
        fuelTank = temp;
        System.out.println("Tank at: " + fuelTank);
        System.out.println("Refueling compleat\n");
    }

    /**
     * in pumpManage we have a menu to choose with pump you will use
     * after you choose a pump you enter the desired amount of gas
     * then the object will check if ther is fuel in the gastank to refuel the desired amount
     * if not the system tells that ther isn't enough fuel
     * ther is olso admin nemu that will return you to the former menu after the right code was enters
     */
    private static void pumpManage(){
        int intTemp = 0;
        boolean temp = true;
        do {
            System.out.println("\nChoose a pump");
            System.out.println("1: pump one");
            System.out.println("2: pump Two");
            System.out.println("3: pump Three");
            System.out.println("4: pump Four");
            System.out.println("5: Admin menu");
            System.out.print("Choice: ");
            switch (sc.next()){
                case "1":
                    System.out.println("\nCurrent tank amount: " + fuelTank);
                    System.out.println("Enter refuel amount");
                    intTemp = sc.nextInt();
                    fuelTank = pump1.pumpCounter(fuelTank,intTemp);
                    break;
                case "2":
                    System.out.println("\nCurrent tank amount: " + fuelTank);
                    System.out.println("Enter refuel amount");
                    intTemp = sc.nextInt();
                    fuelTank = pump2.pumpCounter(fuelTank,intTemp);
                    break;
                case "3":
                    System.out.println("\nCurrent tank amount: " + fuelTank);
                    System.out.println("Enter refuel amount");
                    intTemp = sc.nextInt();
                    fuelTank = pump3.pumpCounter(fuelTank,intTemp);
                    break;
                case "4":
                    System.out.println("\nCurrent tank amount: " + fuelTank);
                    System.out.println("Enter refuel amount");
                    intTemp = sc.nextInt();
                    fuelTank = pump4.pumpCounter(fuelTank,intTemp);
                    break;
                case "5":
                    temp = adminLogin();
                    break;
                default:
                    System.out.println("Wrong input");
                    break;
            }
        }while (temp);
    }

    /**
     * in adminLogin we check if the enterd code if it is the right code and returen true or false depending of the code given
     * if the right code is enterd you return to the first menu
     * @return
     */
    private static boolean adminLogin(){
        String adminCode = "Cake";
        boolean temp;
        System.out.println("\nEnter Code");
        if (adminCode.equals(sc.next())){
            System.out.println("Access granted");
            temp = false;
        } else {
            System.out.println("access denied\n");
            temp = true;
        }
        return temp;
    }
}

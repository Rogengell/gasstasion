public class FuelPump {

    private int counter = 0;

    /**
     * her you return the counter for the amount the pump has used
     * @return
     */
    public int getCounter() {
        return counter;
    }

    /**
     * her we have to parameters "tank" that is the amount current in fueltank
     * and "refuel" that is the desired amount of the user
     * the method checks if ther is enough fuel
     * if ther is it will remove the desired amount from the current gastank
     * if not it will reture the current gas tank with out change
     * @param tank
     * @param reFuel
     * @return
     */
    public int pumpCounter(int tank, int reFuel){
        int temp = tank - reFuel;
        if (temp < 0)
        {
            System.out.println("\nRefuel amount exceeds current tank amount");
            temp = tank;
        }
        else
        {
            counter += reFuel;
        }
        return temp;
    }

}
